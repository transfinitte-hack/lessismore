package com.transfinitte.LessIsMore

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LessIsMoreApplication

fun main(args: Array<String>) {
	runApplication<LessIsMoreApplication>(*args)
}

