package com.transfinitte.LessIsMore.Shared

data class MedicineData (
        val medicineId:Long,
        val productName:String,
        val companyName:String
)