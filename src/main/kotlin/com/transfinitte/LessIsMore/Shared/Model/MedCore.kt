package com.transfinitte.LessIsMore.Shared.Model

import java.util.*
import javax.persistence.*


@Entity
@Table(name = "med_core")
data class MedCore (

        @Id
        @Column(name = "med_id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id:Long? = null,

        @Column(name = "product_name",nullable = false)
        val productName:String,

        @Column(name = "company_name",nullable = false)
        val companyName:String,

        @Column(name = "expiry_date",nullable = false)
        @Temporal(TemporalType.DATE)
        val expiryDate:Date,

        @Column(name = "price_per_item",nullable = false)
        val pricePerItem: Float,

        @Column(nullable = false,name = "med_type")
        @Enumerated(EnumType.STRING)
        val medType:MedType

)