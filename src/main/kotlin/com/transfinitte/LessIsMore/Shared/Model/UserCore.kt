package com.transfinitte.LessIsMore.Shared.Model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "user_core")
data class UserCore (

        @Id
        @Column(name = "user_id")
        val id:String,

        @Column(nullable = false,name = "name",length = 100)
        val name:String,

        @Column(nullable = false,name = "photo_url")
        val photoUrl:String,

        @Column(name="address",nullable = false)
        val address:String,

        @Column(name = "cell_no",length = 20)
        val cellNo:String
)