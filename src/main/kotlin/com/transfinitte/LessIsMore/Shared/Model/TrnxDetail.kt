package com.transfinitte.LessIsMore.Shared.Model

import java.time.LocalDateTime
import javax.persistence.*


@Entity
@Table(name = "trnx_detail")
data class TrnxDetail(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "trnx_id")
        val id:Long? = null,

        @Column(name = "trnx_date_time",nullable = false)
        var trnxDateTime: LocalDateTime? = null,

        @ManyToOne(optional = false,fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        val userCore: UserCore

) {

    @PrePersist
    fun onPrePersist() {

        println("prepersist is executed")
        trnxDateTime = LocalDateTime.now()

    }

}