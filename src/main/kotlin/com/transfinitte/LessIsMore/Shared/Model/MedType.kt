package com.transfinitte.LessIsMore.Shared.Model

enum class MedType {
    STRIP,BOTTLE,PACKET
}