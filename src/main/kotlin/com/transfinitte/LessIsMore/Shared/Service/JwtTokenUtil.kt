package com.transfinitte.LessIsMore.Shared.Service


import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.impl.TextCodec
import io.jsonwebtoken.impl.crypto.MacProvider
import org.springframework.stereotype.Component
import java.io.Serializable
import java.util.*

@Component
class JwtTokenUtil : Serializable{


    // secret key for jwt token signature is generated programmatically
    // this is leading to an error, when the server is restarted all the old tokens becomes invalid because the secret key changes, hence old tokens cant be verified.
    companion object {

        private val key = MacProvider.generateKey(SignatureAlgorithm.HS512)
        private val tempKey = "Transfinitte-jwt-key"
        private val base64secret = TextCodec.BASE64.encode(tempKey)


        @Throws(SignatureException::class)
        fun getAllClaimsFromToken(token: String):Claims {
            return Jwts.parser()
                    .setSigningKey(base64secret)
                    .parseClaimsJws(token)
                    .body

        }


        fun generateToken(): String {


           // println("TOKEN   " + base64secret)

            val uuidStr = UUID.randomUUID().toString()
            //println("UUID_GENERAT "+uuidStr)
            return Jwts.builder()
                    .setSubject(uuidStr)
                    .setIssuedAt(Date())
                    .signWith(SignatureAlgorithm.HS512, base64secret)
                    .compact()
        }


    }


}


//this is used to generate JWT Tokens.
fun main(args:Array<String>) {

//    for(i in 1..10) {
//
//        val token = JwtTokenUtil.generateToken()
//        println(token)
//       // println(JwtTokenUtil.getAllClaimsFromToken(token).subject)
//
//    }

    println(JwtTokenUtil.getAllClaimsFromToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI1ODc2YzFiNy02MThjLTRlYTAtYTFmNC1kOThjYzIwY2VhMjYiLCJpYXQiOjE1NDk2OTI3NTh9.KH4abn7nDDjFsBoZ8MgJ2_QsaQSTPLRsWg5sUFo0E6LQ-2kMBlE6VKAYvtg9XqdFS4f36jkWYJazk6WS2U4PDw").subject)



}