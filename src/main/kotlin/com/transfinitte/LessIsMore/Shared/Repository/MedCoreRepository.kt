package com.transfinitte.LessIsMore.Shared.Repository

import com.transfinitte.LessIsMore.Shared.Model.MedCore
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MedCoreRepository : JpaRepository<MedCore,Long>{
}