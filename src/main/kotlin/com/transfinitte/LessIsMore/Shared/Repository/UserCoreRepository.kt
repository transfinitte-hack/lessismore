package com.transfinitte.LessIsMore.Shared.Repository

import com.transfinitte.LessIsMore.Shared.Model.UserCore
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserCoreRepository : JpaRepository<UserCore,String> {
}