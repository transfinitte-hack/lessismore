package com.transfinitte.LessIsMore.Shared.Repository

import com.transfinitte.LessIsMore.Shared.Model.TrnxDetail
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TrnxDetailRepository : JpaRepository<TrnxDetail,Long>{
}