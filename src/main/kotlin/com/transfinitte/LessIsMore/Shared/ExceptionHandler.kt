package com.transfinitte.LessIsMore.Shared

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(val msg:String) : RuntimeException(msg)

//@ControllerAdvice
//@RestController
//class CustomizedResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
//
//    @ExceptionHandler(BadRequestException::class)
//    fun handleUserNotFoundException(ex:BadRequestException,request: WebRequest): ResponseEntity<BadQuestionError> {
//        return ResponseEntity(ex.errorObj,HttpStatus.BAD_REQUEST)
//    }
//}