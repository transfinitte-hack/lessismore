package com.transfinitte.LessIsMore.Shared

import com.transfinitte.LessIsMore.Shared.Model.UserCore
import com.transfinitte.LessIsMore.Shared.Repository.MedCoreRepository
import com.transfinitte.LessIsMore.Shared.Repository.UserCoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
class SharedController {

    @Autowired
    lateinit var userCoreRepository: UserCoreRepository

    @Autowired
    lateinit var medCoreRepository: MedCoreRepository

    @RequestMapping("user/{id}",method = [RequestMethod.GET])
    fun getUserData(@PathVariable("id") userId:String) : UserCore {
        return userCoreRepository.findById(userId).orElseThrow { BadRequestException("Invalid UserId") }
    }

    @RequestMapping("medicines",method = [RequestMethod.GET])
    fun getAllMedicines() : List<MedicineData> {
       return medCoreRepository.findAll().map {
            MedicineData(
                    medicineId = it.id!!,
                    productName = it.productName,
                    companyName = it.companyName
            )
        }
    }
}