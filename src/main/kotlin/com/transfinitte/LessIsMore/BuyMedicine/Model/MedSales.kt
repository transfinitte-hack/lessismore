package com.transfinitte.LessIsMore.BuyMedicine.Model

import com.transfinitte.LessIsMore.Shared.Model.MedCore
import com.transfinitte.LessIsMore.Shared.Model.TrnxDetail
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "med_sales")
data class MedSales (

        @Id
        @Type(type = "uuid-char")
        val uuid:UUID,

        @ManyToOne(optional = false,fetch = FetchType.LAZY)
        @JoinColumn(name = "trnx_id")
        val trnxDetail:TrnxDetail,

        @ManyToOne(optional = false,fetch = FetchType.LAZY)
        @JoinColumn(name = "med_id")
        val medCore:MedCore,

        @Column(name = "item_count",nullable = false)
        val itemCount:Int

)