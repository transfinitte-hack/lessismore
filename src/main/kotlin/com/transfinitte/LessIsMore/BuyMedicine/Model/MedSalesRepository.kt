package com.transfinitte.LessIsMore.BuyMedicine.Model

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface MedSalesRepository : JpaRepository<MedSales,UUID> {
}