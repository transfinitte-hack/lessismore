package com.transfinitte.LessIsMore.BuyMedicine

import com.transfinitte.LessIsMore.BuyMedicine.Service.BuyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
class BuyController {

    @Autowired
    lateinit var buyService: BuyService

    @RequestMapping("buy/{id}/submit",method = [RequestMethod.POST])
    fun submitBuyRequest(@PathVariable("id") userId:String, @RequestBody itemList:List<BuyRequestItem>) : BuyResponseData {
        return buyService.onNewBuyRequest(userId = userId,itemsBuyReq = itemList)
    }

}