package com.transfinitte.LessIsMore.BuyMedicine

import java.util.*

data class BuyRequestItem(
    val medicineId:Long,
    val itemCount:Int,
    val uuidToken:String
)

data class BuyResponseData(
        val trnxId:Long,
        val itemsBought:List<BoughtItem>
)

data class BoughtItem(
        val productName:String,
        val companyName:String,
        val itemCount:Int,
        val expiryDate:Date
)



