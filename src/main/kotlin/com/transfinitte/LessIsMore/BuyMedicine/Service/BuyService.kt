package com.transfinitte.LessIsMore.BuyMedicine.Service

import com.transfinitte.LessIsMore.BuyMedicine.BuyRequestItem
import com.transfinitte.LessIsMore.BuyMedicine.BuyResponseData

interface BuyService {

    fun onNewBuyRequest(userId:String,itemsBuyReq:List<BuyRequestItem>) : BuyResponseData
}