package com.transfinitte.LessIsMore.BuyMedicine.Service

import com.transfinitte.LessIsMore.BuyMedicine.BoughtItem
import com.transfinitte.LessIsMore.BuyMedicine.BuyRequestItem
import com.transfinitte.LessIsMore.BuyMedicine.BuyResponseData
import com.transfinitte.LessIsMore.BuyMedicine.Model.MedSales
import com.transfinitte.LessIsMore.BuyMedicine.Model.MedSalesRepository
import com.transfinitte.LessIsMore.Shared.BadRequestException
import com.transfinitte.LessIsMore.Shared.Model.TrnxDetail
import com.transfinitte.LessIsMore.Shared.Repository.MedCoreRepository
import com.transfinitte.LessIsMore.Shared.Repository.TrnxDetailRepository
import com.transfinitte.LessIsMore.Shared.Repository.UserCoreRepository
import com.transfinitte.LessIsMore.Shared.Service.JwtTokenUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class BuyServiceImpl : BuyService {

    @Autowired
    private lateinit var userCoreRepository: UserCoreRepository

    @Autowired
    private lateinit var trnxDetailRepository: TrnxDetailRepository

    @Autowired
    private lateinit var medSalesRepository: MedSalesRepository

    @Autowired
    private lateinit var medCoreRepository: MedCoreRepository

    override fun onNewBuyRequest(userId: String, itemsBuyReq: List<BuyRequestItem>): BuyResponseData {

        try {

            val userCore = userCoreRepository.findById(userId).get()
            val trnxDetail:TrnxDetail = trnxDetailRepository.save(TrnxDetail(userCore = userCore))

            val boughtItemList: MutableList<BoughtItem> = mutableListOf()
            itemsBuyReq.forEach {
                val tokenClaims = JwtTokenUtil.getAllClaimsFromToken(it.uuidToken)
                val itemUUID: UUID =  UUID.fromString(tokenClaims.subject)

                val medCore = medCoreRepository.findById(it.medicineId).get()

                medSalesRepository.save(MedSales(
                        uuid = itemUUID,
                        trnxDetail = trnxDetail,
                        itemCount = it.itemCount,
                        medCore = medCore
                ))

                boughtItemList.add(BoughtItem(
                        productName = medCore.productName,
                        companyName = medCore.companyName,
                        expiryDate = medCore.expiryDate,
                        itemCount = it.itemCount
                ))

            }

            return BuyResponseData(
                    trnxId = trnxDetail.id!!,
                    itemsBought = boughtItemList
            )

        } catch(ex:Exception) {
            throw BadRequestException("Unable to Process")
        }

    }
}