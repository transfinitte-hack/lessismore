package com.transfinitte.LessIsMore.ReturnMedicine.Model

enum class ReturnAction{
    ADD_TO_INVENTORY,DISPOSE,DECIDE_LATER
}