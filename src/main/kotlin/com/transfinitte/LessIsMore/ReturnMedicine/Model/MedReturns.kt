package com.transfinitte.LessIsMore.ReturnMedicine.Model

import com.transfinitte.LessIsMore.BuyMedicine.Model.MedSales
import com.transfinitte.LessIsMore.Shared.Model.TrnxDetail
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "med_returns")
data class MedReturns(

        @Id
        @Type(type = "uuid-char")
        val uuid: UUID?= null,

        @ManyToOne(optional = false,fetch = FetchType.LAZY)
        @JoinColumn(name = "trnx_id")
        val trnxDetail: TrnxDetail,

        @OneToOne(fetch = FetchType.LAZY,optional = false)
        @MapsId
        val medSales: MedSales,

        @Column(name = "item_count",nullable = false)
        val itemCount:Int,

        @Column(nullable = false,name = "return_action")
        @Enumerated(EnumType.STRING)
        val actionTaken: ReturnAction

)