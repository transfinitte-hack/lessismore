package com.transfinitte.LessIsMore.ReturnMedicine.Model

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface MedReturnsRepository : JpaRepository<MedReturns,UUID> {
}