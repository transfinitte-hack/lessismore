package com.transfinitte.LessIsMore.ReturnMedicine.Service

import com.transfinitte.LessIsMore.ReturnMedicine.BuyHistoryData
import com.transfinitte.LessIsMore.ReturnMedicine.ReturnRequestItem
import com.transfinitte.LessIsMore.ReturnMedicine.ReturnResponseData

interface ReturnService {

    fun verifyProductToken(tokenStr:String) : BuyHistoryData

    fun onNewReturnRequest(userId:String,itemsReturnReq:List<ReturnRequestItem>) : ReturnResponseData

}