package com.transfinitte.LessIsMore.ReturnMedicine.Service

import com.transfinitte.LessIsMore.BuyMedicine.BoughtItem
import com.transfinitte.LessIsMore.BuyMedicine.Model.MedSalesRepository
import com.transfinitte.LessIsMore.ReturnMedicine.BuyHistoryData
import com.transfinitte.LessIsMore.ReturnMedicine.Model.MedReturns
import com.transfinitte.LessIsMore.ReturnMedicine.Model.MedReturnsRepository
import com.transfinitte.LessIsMore.ReturnMedicine.ReturnRequestItem
import com.transfinitte.LessIsMore.ReturnMedicine.ReturnResponseData
import com.transfinitte.LessIsMore.ReturnMedicine.ReturnedItem
import com.transfinitte.LessIsMore.Shared.BadRequestException
import com.transfinitte.LessIsMore.Shared.Model.TrnxDetail
import com.transfinitte.LessIsMore.Shared.Repository.TrnxDetailRepository
import com.transfinitte.LessIsMore.Shared.Repository.UserCoreRepository
import com.transfinitte.LessIsMore.Shared.Service.JwtTokenUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.ZoneId
import java.util.*


@Service
class ReturnServiceImpl : ReturnService{

    @Autowired
    private lateinit var medSalesRepository: MedSalesRepository

    @Autowired
    private lateinit var userCoreRepository: UserCoreRepository

    @Autowired
    private lateinit var trnxDetailRepository: TrnxDetailRepository

    @Autowired
    private lateinit var medReturnsRepository : MedReturnsRepository

    override fun verifyProductToken(tokenStr: String) : BuyHistoryData {

        try {

            println("token $tokenStr")
            val claims = JwtTokenUtil.getAllClaimsFromToken(tokenStr)
            val uuid:UUID = UUID.fromString(claims.subject)

            println("UUID "+uuid.toString())

            val medSales = medSalesRepository.findById(uuid).get()

            return BuyHistoryData(
                    uuid = uuid,
                    productName = medSales.medCore.productName,
                    companyName = medSales.medCore.companyName,
                    expiryDate = medSales.medCore.expiryDate,
                    itemCount = medSales.itemCount,
                    boughtByUserId = medSales.trnxDetail.userCore.id,
                    buyDate = Date.from(medSales.trnxDetail.trnxDateTime!!.atZone(ZoneId.systemDefault()).toInstant())
            )

        }catch (ex:Exception) {
            throw BadRequestException("Invalid Product !")
        }
    }

    override fun onNewReturnRequest(userId: String, itemsReturnReq: List<ReturnRequestItem>): ReturnResponseData {

        try {
            val userCore = userCoreRepository.findById(userId).get()
            val trnxDetail: TrnxDetail = trnxDetailRepository.save(TrnxDetail(userCore = userCore))

            val returnedItemList: MutableList<ReturnedItem> = mutableListOf()

            itemsReturnReq.forEach {

                val medSaleEntry = medSalesRepository.findById(it.uuid).get()

                if(medReturnsRepository.findById(it.uuid).isPresent)
                    throw BadRequestException("Product Already Returned Once, Fake User!")

                medReturnsRepository.save(MedReturns(
                        medSales = medSaleEntry,
                        itemCount = it.itemCount,
                        trnxDetail = trnxDetail,
                        actionTaken = it.actionTaken
                ))

                returnedItemList.add(ReturnedItem(
                        productName = medSaleEntry.medCore.productName,
                        companyName = medSaleEntry.medCore.companyName,
                        itemCount = it.itemCount
                ))

            }

            return ReturnResponseData(
                    trnxId = trnxDetail.id!!,
                    itemsReturned = returnedItemList
            )

        } catch (ex:Exception) {
            throw BadRequestException("Invalid Data!")
        }

    }
}