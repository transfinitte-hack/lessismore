package com.transfinitte.LessIsMore.ReturnMedicine

import com.transfinitte.LessIsMore.BuyMedicine.BuyRequestItem
import com.transfinitte.LessIsMore.BuyMedicine.BuyResponseData
import com.transfinitte.LessIsMore.ReturnMedicine.Service.ReturnService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
class ReturnController {

    @Autowired
    private lateinit var returnService: ReturnService

    @RequestMapping("/return/qrVerify",method = [RequestMethod.POST])
    fun verifyProductToken(@RequestBody tokenStr:String) : BuyHistoryData {
        return returnService.verifyProductToken(tokenStr)
    }

    @RequestMapping("/return/{id}/submit",method = [RequestMethod.POST])
    fun returnVerifiedProducts(@PathVariable("id") userId:String, @RequestBody itemList:List<ReturnRequestItem>) : ReturnResponseData {
        return returnService.onNewReturnRequest(userId,itemList)
    }



}