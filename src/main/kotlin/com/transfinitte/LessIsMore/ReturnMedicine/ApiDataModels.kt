package com.transfinitte.LessIsMore.ReturnMedicine

import com.transfinitte.LessIsMore.ReturnMedicine.Model.ReturnAction
import java.util.*

data class BuyHistoryData(

        val productName:String,
        val companyName:String,
        val expiryDate:Date,
        val itemCount:Int,
        val boughtByUserId:String,
        val buyDate:Date,
        val uuid:UUID
)

data class ReturnRequestItem(
        val uuid:UUID,
        val actionTaken: ReturnAction,
        val itemCount:Int
)




data class ReturnResponseData(
        val trnxId:Long,
        val itemsReturned: List<ReturnedItem>
)

data class ReturnedItem(
        val productName:String,
        val companyName: String,
        val itemCount:Int
)