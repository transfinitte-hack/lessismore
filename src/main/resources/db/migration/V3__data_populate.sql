
INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (2,'Modi Mundi Pharma Pvt Ltd','STRIP','VALCONTIN 200 MG TABLET',DATE '2021-01-01',3.7);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (3,'Cipla Ltd','STRIP','Paracip 650 Tablet',DATE '2019-02-01',1.90);


INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (4,'Mankind Pharma Ltd','STRIP','Zenflox-OZ Tablet',DATE '2020-01-01',8.90);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (5,'Pfizer Ltd','STRIP','Becosules Z Capsule',DATE '2022-01-01',1.20);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (6,'Lupin Ltd','STRIP','L-Cin 500 Tablet',DATE '2022-10-01',8.70);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (7,'Medley Pharmaceuticals','STRIP','O2 Tablet',DATE '2019-02-15',11.20);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (8,'
Glaxo SmithKline Pharmaceuticals Ltd','STRIP','Zinetac 300 mg Tablet',DATE '2019-03-01',1.60);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (9,'
Knoll Pharmaceuticals Ltd','STRIP','Cetrizine Tablet',DATE '2020-01-01',2.30);

INSERT INTO med_core(med_id,company_name,med_type,product_name,expiry_date,price_per_item)
VALUES (10,'
Sarmain Pharmaceuticals','STRIP','Motinac 50mg Tablet',DATE '2021-01-01',6.30);
